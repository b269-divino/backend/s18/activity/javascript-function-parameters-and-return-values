// console.log("Hello World!");


	
	// 1.  Create a function which will be able to add two numbers.
	// 	-Numbers must be provided as arguments.
	// 	-Display the result of the addition in our console.
	// 	-function should only display result. It should not return anything.

	// 	Create a function which will be able to subtract two numbers.
	// 	-Numbers must be provided as arguments.
	// 	-Display the result of subtraction in our console.
	// 	-function should only display result. It should not return anything.

	// 	-invoke and pass 2 arguments to the addition function
	// 	-invoke and pass 2 arguments to the subtraction function

function number(num1){
    let resultAdd = num1 + 15;
    let resultSub = 20 - num1;
    console.log("Displayed sum of 5 and 15:");
    console.log(resultAdd);
    console.log("Displayed difference of 20 and 5:");
    console.log(resultSub);
}

number(5);


// 2.  Create a function which will be able to multiply two numbers.
// 		-Numbers must be provided as arguments.
// 		-Return the result of the multiplication.

// 	Create a function which will be able to divide two numbers.
// 		-Numbers must be provided as arguments.
// 		-Return the result of the division.

//  	Create a global variable called outside of the function called product.
// 		-This product variable should be able to receive and store the result of multiplication function.
// 	Create a global variable called outside of the function called quotient.
// 		-This quotient variable should be able to receive and store the result of division function.

// 	Log the value of product variable in the console.
// 	Log the value of quotient variable in the console.

function number1(number){
    let resultMultiply = number * 10;
    let resultDivide = number / 10;
    console.log("The product of 50 and 10:");
    console.log(resultMultiply);
    console.log("The quotient of 50 and 10:");
    console.log(resultDivide);
}

number1(50);



// 3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
// 		-a number should be provided as an argument.
// 		-look up the formula for calculating the area of a circle with a provided/given radius.
// 		-look up the use of the exponent operator.
// 		-you can save the value of the calculation in a variable.
// 		-return the result of the area calculation.

// 	Create a global variable called outside of the function called circleArea.
// 		-This variable should be able to receive and store the result of the circle area calculation.

// Log the value of the circleArea variable in the console.

// The result of getting the area of a circle with 15 radius:

function numberCircle(radius){
    let circle = (radius * radius) * 3.14159265;
    console.log("The result of getting the area of a circle with 15 radius:");
    console.log(circle);

}

numberCircle(15);



// 4. 	Create a function which will be able to get total average of four numbers.
// 		-4 numbers should be provided as an argument.
// 		-look up the formula for calculating the average of numbers.
// 		-you can save the value of the calculation in a variable.
// 		-return the result of the average calculation.

//     Create a global variable called outside of the function called averageVar.
// 		-This variable should be able to receive and store the result of the average calculation
// 		-Log the value of the averageVar variable in the console.


function average(var1, var2, var3, var4){
	let averageVar = (var1 + var2 + var3 + var4) / 4; 
	return averageVar;
}

let myAverage = average(20, 40, 60, 80);
console.log("The average of 20, 40, 60 and 80:");
console.log(myAverage);



// 5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
// 		-this function should take 2 numbers as an argument, your score and the total score.
// 		-First, get the percentage of your score against the total. You can look up the formula to get percentage.
// 		-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
// 		-return the value of the variable isPassed.
// 		-This function should return a boolean.

// 	Create a global variable called outside of the function called isPassingScore.
// 		-This variable should be able to receive and store the boolean result of the checker function.
// 		-Log the value of the isPassingScore variable in the console.




function percentage(num){
    let remainder = num % 38;
    console.log("is 38/50 a passing score?");
    let isPassed = remainder <= 75;
    console.log(isPassed);
}

percentage(50);
